from flask.globals import request
from flask import jsonify, json
from flask.templating import render_template
from ezicd import app


@app.route('/')
def landing():
    return render_template('landing.html')
