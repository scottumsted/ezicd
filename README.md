# EZICD #

This is the flask version (as opposed to the JMVC version) of the ICD10 lookup omnibox. The site is accessible at http://ezicd.ezpz.gs.

The interesting code is the script at the bottom of the templates/landing.html file.

EZICD uses the icd10 lookup api at http://api.wuppsy.com. The api repo and icd10 load/indexing is not public.